using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesPool : MonoBehaviour
{
    // Start is called before the first frame
    [SerializeField] private GameObject[] obstaclesPrefab;
    GameObject obstaclesbool;
    private int poolSize = 2;
    private GameObject[] obstacles;
    private float  spawnTime = 2.5f;
    private float timeElapsed;
    private int obstacleCount;
    [Space, SerializeField] private float timeToSpawnFirstPipe;
    [SerializeField] private float timeToSpawnPipe;

    [Space, SerializeField] private Transform pipeSpawnPosition;

    [Space, SerializeField] private Transform pipeMinSpawnHeight;
    [SerializeField] private Transform pipeMaxSpawnHeight;

 
    void Start()
    {
        obstacles = new GameObject[poolSize];
        
        for (int i = 0; i < poolSize; i++)
        {
            obstacles[i] = Instantiate(obstaclesPrefab[GameManager.dia]);
            obstacles[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed > spawnTime)
        {
            SpawnObstacle();
        }
    }
    void SpawnObstacle()
    {
        timeElapsed = 0f;
        float ySpawnPosition= Random.Range(pipeMinSpawnHeight.position.y, pipeMaxSpawnHeight.position.y);
        Vector2 spawnPosition = new Vector2(pipeSpawnPosition.position.x, ySpawnPosition);
        obstacles[obstacleCount].transform.position = spawnPosition;
        obstacles[obstacleCount].SetActive(true);
        obstacleCount++;
        if(obstacleCount == poolSize) {
            obstacleCount = 0;
        }
    }

}
