using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeBackground : MonoBehaviour
{
    // Start is called before the first frame update
    public Sprite[] newSprite;
    public GameManager gameManager;
    public int fondo;// Asignar el nuevo sprite aqu�

    public void ReplaceSprite()
    {
        fondo = GameManager.dia;


        // Obtener el componente SpriteRenderer del objeto
        SpriteRenderer spriteRenderer = this.GetComponent<SpriteRenderer>();

        // Reemplazar el sprite actual por el nuevo sprite
        spriteRenderer.sprite = newSprite[fondo];
    
    
    }
}
