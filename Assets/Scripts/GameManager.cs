using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private float timeToReloadScene;
    static public  int dia;
    static public int skinPlayer;
    private int compareskinovertime;
    //public static GameManager highscoretext;
    public static int highscore;
    [Space, SerializeField] private Transform playerSpawnPosition;
    public GameObject player;

    [Space, SerializeField] private UnityEvent onStartGame;
    [SerializeField] private UnityEvent onGameOver, onIncreaseScore;

    public int score
    {
        get;
        private set;
    }

    public bool isGameOver
    {
        get;
        private set;
    }

    // Singleton!
    public static GameManager Instance;

    private void Awake()
    {
      
       //Instance = this;
        if (Instance == null)
            Instance = this;
        else
            DestroyImmediate(this.gameObject);
    }
    private void Start()
    {
        highscore = PlayerPrefs.GetInt("highscore", 0);
    }
    public void StartGame()
    {
        

        dia = Random.Range(0, 2);
      

        //while (compareskinovertime == skinPlayer)
        //{
        //    skinPlayer = Random.Range(0, 3);
        //}
        Debug.Log("GameManager :: StartGame()");

        onStartGame?.Invoke();
    }
    private Vector3 GetPlayerPosition()
    {
        return new Vector3(playerSpawnPosition.position.x, playerSpawnPosition.position.y, playerSpawnPosition.position.z);
    }
    private void SpawnPlayer()
    {
        Debug.Log("PipeSpawner :: SpawnPipe()");

        Instantiate(player, GetPlayerPosition(), Quaternion.identity);
    }


    public void GameOver()
    {
       
        
        if (isGameOver)
            return;
       

        Debug.Log("GameManager :: GameOver()");

        isGameOver = true;

        onGameOver?.Invoke();
        
        StartCoroutine(ReloadScene());
       
    }

    public void IncreaseScore()
    {
        Debug.Log("GameManager :: IncreaseScore()");
        score++;
        onIncreaseScore?.Invoke();
        if (highscore < score)
        {
            PlayerPrefs.SetInt("highscore", score);
        }
    }

    private IEnumerator ReloadScene()
    {
        yield return new WaitForSeconds(timeToReloadScene);

        Debug.Log("GameManager :: ReloadScene()");

        SceneManager.LoadScene(0);
    }
    
}
