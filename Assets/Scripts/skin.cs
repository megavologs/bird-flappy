using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class skin : MonoBehaviour
{
    public AnimatorOverrideController blueanim;
    public AnimatorOverrideController redanim;
    public AnimatorOverrideController yellowanim;
    private int skinnumb;

    // Start is called before the first frame update
    void Start()
    {
        skinnumb = Random.Range(0, 2);
        if (skinnumb == 0)
            blueskin();
        if (skinnumb == 1)
            redskin();
        if(skinnumb == 2)
            yellowskin();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void blueskin()
    {
        GetComponent<Animator>().runtimeAnimatorController = blueanim as RuntimeAnimatorController;   }
    public void redskin()
    {
        GetComponent<Animator>().runtimeAnimatorController = redanim as RuntimeAnimatorController;
    }
    public void yellowskin()
    {
        GetComponent<Animator>().runtimeAnimatorController = yellowanim as RuntimeAnimatorController;
    }
}
